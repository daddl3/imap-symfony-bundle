# IMAP Symfony Bundle

[![Pipeline Status](https://gitlab.com/daddl3/imap-symfony-bundle/badges/0.0.1/pipeline.svg)]()
[![Version](https://img.shields.io/badge/version-0.0.1-blue)](https://gitlab.com/daddl3/vite-and-twig-compression-symfony-bundle/blob/master/version.txt)

#### Attention

to avoid to execute Javascript from your E-Mails use this

```HTML
<meta http-equiv="Content-Security-Policy" content="default-src 'none'; img-src 'self'; style-src 'self';">
```
