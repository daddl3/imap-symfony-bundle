<?php

namespace daddl3\ImapSymfonyBundle\Service;

use Webklex\PHPIMAP\ClientManager;
use Webklex\PHPIMAP\Support\MessageCollection;

readonly class ImapService
{
    /**
     * @param array<string,int|array<string, int>> $config
     */
    public function __construct(
        private array $config
    ) {
    }

    /**
     * @return array<int|string,array<int, mixed>>
     */
    public function getEmailInbox(?string $account = null): array
    {
        $clientManager = $this->getClientManager();

        if (null !== $account) {
            /* set default mail account, because $client->account($account) not working well */
            $clientManager->setDefaultAccount($account);
        }

        $clientManager->connect();

        $emails = $this->fetchAllEmails(
            clientManager: $clientManager,
            folders: ['INBOX', 'Sent']
        );

        return $this->organizeEmailsByAddress(
            allEmails: $emails
        );
    }

    private function getClientManager(): ClientManager
    {
        return new ClientManager($this->config);
    }

    /**
     * @param array<string> $folders
     *
     * @return array<string,MessageCollection>
     */
    private function fetchAllEmails(
        ClientManager $clientManager,
        array $folders
    ): array {
        $returnArray = [];
        foreach ($folders as $folder) {
            $returnArray[$folder] = $clientManager->getFolder($folder)->messages()->all()->get();
        }

        return $returnArray;
    }

    /**
     * @param array<string,MessageCollection> $allEmails
     *
     * @return array<int|string,array<int, mixed>>
     *
     * This function identifies related emails and groups them together,
     * making it easier to track conversations and email threads
     */
    private function organizeEmailsByAddress(array $allEmails): array
    {
        $organizedEmails = [];

        if (\array_key_exists('INBOX', $allEmails)) {
            foreach ($allEmails['INBOX'] as $email) {
                $fromAddress = $email->from[0]->mail;

                if (!isset($organizedEmails[$fromAddress])) {
                    $organizedEmails[$fromAddress] = [];
                }

                $organizedEmails[$fromAddress][] = $email;
            }
        }

        if (\array_key_exists('Sent', $allEmails)) {
            foreach ($allEmails['Sent'] as $email) {
                $toAddress = $email->to[0]->mail;

                if (!isset($organizedEmails[$toAddress])) {
                    $organizedEmails[$toAddress] = [];
                }

                $organizedEmails[$toAddress][] = $email;
            }
        }

        /* Sort the emails by date for each address */
        foreach ($organizedEmails as $address => $emails) {
            usort($emails, static fn ($a, $b): int => $a->date <=> $b->date);

            $organizedEmails[$address] = $emails;
        }

        return $organizedEmails;
    }
}
