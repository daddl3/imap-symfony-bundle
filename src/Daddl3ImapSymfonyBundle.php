<?php

namespace daddl3\ImapSymfonyBundle;

use Override;
use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;
use Symfony\Component\Config\Definition\Builder\NodeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;
use Symfony\Component\Config\Definition\Configurator\DefinitionConfigurator;
use Symfony\Component\Config\Definition\Processor;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use Symfony\Component\HttpKernel\Bundle\AbstractBundle;
use Webklex\PHPIMAP\Events\FlagDeletedEvent;
use Webklex\PHPIMAP\Events\FlagNewEvent;
use Webklex\PHPIMAP\Events\FolderDeletedEvent;
use Webklex\PHPIMAP\Events\FolderMovedEvent;
use Webklex\PHPIMAP\Events\FolderNewEvent;
use Webklex\PHPIMAP\Events\MessageCopiedEvent;
use Webklex\PHPIMAP\Events\MessageDeletedEvent;
use Webklex\PHPIMAP\Events\MessageMovedEvent;
use Webklex\PHPIMAP\Events\MessageNewEvent;
use Webklex\PHPIMAP\Events\MessageRestoredEvent;
use Webklex\PHPIMAP\IMAP;
use Webklex\PHPIMAP\Support\Masks\AttachmentMask;
use Webklex\PHPIMAP\Support\Masks\MessageMask;

class Daddl3ImapSymfonyBundle extends AbstractBundle
{
    #[Override]
    public function configure(DefinitionConfigurator $definitionConfigurator): void
    {
        /** @var ArrayNodeDefinition $nodeDefinition */
        $nodeDefinition = $definitionConfigurator->rootNode();

        /** @var NodeBuilder $children */
        $children = $nodeDefinition
            ->children();

        /* @phpstan-ignore-next-line */
        $children
            ->scalarNode('date_format')
                ->defaultValue('d-M-Y')
            ->end()
            ->scalarNode('default')
                ->defaultValue('default')
            ->end()
            ->arrayNode('accounts')
                ->arrayPrototype()
                    ->children()
                        ->scalarNode('host')
                            ->isRequired()
                        ->end()
                        ->integerNode('port')
                            ->isRequired()
                        ->end()
                        ->scalarNode('protocol')
                            ->defaultValue('imap')
                        ->end()
                        ->scalarNode('encryption')
                            ->isRequired()
                        ->end()
                        ->booleanNode('validate_cert')
                            ->defaultTrue()
                        ->end()
                        ->scalarNode('username')
                            ->isRequired()
                        ->end()
                        ->scalarNode('password')
                            ->isRequired()
                        ->end()
                        ->scalarNode('authentication')
                            ->defaultNull()
                        ->end()
                        ->arrayNode('proxy')
                            ->children()
                                ->scalarNode('socket')
                                    ->defaultNull()
                                ->end()
                                ->booleanNode('request_fulluri')
                                    ->defaultFalse()
                                ->end()
                                ->scalarNode('username')
                                    ->defaultNull()
                                ->end()
                                ->scalarNode('password')
                                    ->defaultNull()
                                ->end()
                            ->end()
                        ->end()
                        ->integerNode('timeout')
                            ->defaultValue(30)
                        ->end()
                        ->arrayNode('extensions')
                            ->scalarPrototype()
                            ->end()
                        ->end()
                    ->end()
                ->end()
            ->end()
            ->arrayNode('options')
                ->children()
                    ->scalarNode('delimiter')
                        ->defaultValue('/')
                    ->end()
                    ->scalarNode('fetch')
                        ->defaultValue(IMAP::FT_PEEK)
                    ->end()
                    ->scalarNode('sequence')
                        ->defaultValue(IMAP::ST_UID)
                    ->end()
                    ->booleanNode('fetch_body')
                        ->defaultTrue()
                    ->end()
                    ->booleanNode('fetch_flags')
                        ->defaultTrue()
                    ->end()
                    ->booleanNode('soft_fail')
                        ->defaultFalse()
                    ->end()
                    ->booleanNode('rfc822')
                        ->defaultTrue()
                    ->end()
                    ->booleanNode('debug')
                        ->defaultFalse()
                    ->end()
                    ->booleanNode('uid_cache')
                        ->defaultTrue()
                    ->end()
                    ->scalarNode('boundary')
                        ->defaultValue('/boundary=(.*?(?=;)|(.*))/i')
                    ->end()
                    ->scalarNode('message_key')
                        ->defaultValue('list')
                    ->end()
                    ->scalarNode('fetch_order')
                        ->defaultValue('asc')
                    ->end()
                    ->arrayNode('dispositions')
                        ->scalarPrototype()
                        ->end()
                        ->defaultValue(['attachment', 'inline'])
                    ->end()
                    ->arrayNode('common_folders')
                        ->children()
                            ->scalarNode('root')
                                ->defaultValue('INBOX')
                            ->end()
                            ->scalarNode('junk')
                                ->defaultValue('INBOX/Junk')
                            ->end()
                            ->scalarNode('draft')
                                ->defaultValue('INBOX/Drafts')
                            ->end()
                            ->scalarNode('sent')
                                ->defaultValue('INBOX/Sent')
                            ->end()
                            ->scalarNode('trash')
                                ->defaultValue('INBOX/Trash')
                            ->end()
                        ->end()
                    ->end()
                    ->arrayNode('decoder')
                        ->children()
                            ->scalarNode('message')
                                ->defaultValue('utf-8')
                            ->end()
                            ->scalarNode('attachment')
                                ->defaultValue('utf-8')
                            ->end()
                        ->end()
                    ->end()
                    ->arrayNode('open')
                    ->end()
                ->end()
            ->end()
            ->arrayNode('flags')
                ->scalarPrototype()
                ->end()
                ->defaultValue(['recent', 'flagged', 'answered', 'deleted', 'seen', 'draft'])
            ->end()
            ->arrayNode('events')
                ->children()
                    ->arrayNode('message')
                        ->children()
                            ->scalarNode('new')
                                ->defaultValue(MessageNewEvent::class)
                            ->end()
                            ->scalarNode('moved')
                                ->defaultValue(MessageMovedEvent::class)
                            ->end()
                            ->scalarNode('copied')
                                ->defaultValue(MessageCopiedEvent::class)
                            ->end()
                            ->scalarNode('deleted')
                                ->defaultValue(MessageDeletedEvent::class)
                            ->end()
                            ->scalarNode('restored')
                                ->defaultValue(MessageRestoredEvent::class)
                            ->end()
                        ->end()
                    ->end()
                    ->arrayNode('folder')
                        ->children()
                            ->scalarNode('new')
                                ->defaultValue(FolderNewEvent::class)
                            ->end()
                            ->scalarNode('moved')
                                ->defaultValue(FolderMovedEvent::class)
                            ->end()
                            ->scalarNode('deleted')
                                ->defaultValue(FolderDeletedEvent::class)
                            ->end()
                        ->end()
                    ->end()
                    ->arrayNode('flag')
                        ->children()
                            ->scalarNode('new')
                                ->defaultValue(FlagNewEvent::class)
                            ->end()
                            ->scalarNode('deleted')
                                ->defaultValue(FlagDeletedEvent::class)
                            ->end()
                        ->end()
                    ->end()
                ->end()
            ->end()
            ->arrayNode('masks')
                ->children()
                    ->scalarNode('message')
                        ->defaultValue(MessageMask::class)
                    ->end()
                    ->scalarNode('attachment')
                        ->defaultValue(AttachmentMask::class)
                    ->end()
                ->end()
            ->end()
        ->end();
    }

    /**
     * @param array<string, int> $config
     */
    #[Override]
    public function loadExtension(array $config, ContainerConfigurator $containerConfigurator, ContainerBuilder $containerBuilder): void
    {
        $containerConfigurator->import('../config/services.yaml');
        $containerBuilder->setParameter('daddl3_imap_symfony_bundle', $config);
    }

    /**
     * @param array<string, int|array<string, int>> $configs
     *
     * @return array<string, int|array<string, int>>
     */
    final protected function processConfiguration(ConfigurationInterface $configuration, array $configs): array
    {
        return (new Processor())->processConfiguration($configuration, $configs);
    }
}
