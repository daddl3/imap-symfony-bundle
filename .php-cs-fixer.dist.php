<?php

$finder = (new PhpCsFixer\Finder())
    ->in(__DIR__)
    ->exclude(['var']);

return (new PhpCsFixer\Config())
    ->setRules([
        '@Symfony' => true,
        '@Symfony:risky' => true,
        'global_namespace_import' => true,
        'explicit_string_variable' => true,
        'ordered_class_elements' => true,
    ])
    ->setRiskyAllowed(true)
    ->setFinder($finder);
