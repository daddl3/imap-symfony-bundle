<?php

namespace daddl3\ImapSymfonyBundle\Twig\Extension;

use daddl3\ImapSymfonyBundle\Twig\Runtime\EmailCreationExtensionRuntime;
use Override;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class EmailCreationExtension extends AbstractExtension
{
    #[Override]
    public function getFunctions(): array
    {
        return [
            new TwigFunction('getEmailInbox', [EmailCreationExtensionRuntime::class, 'getEmailInbox']),
        ];
    }
}
