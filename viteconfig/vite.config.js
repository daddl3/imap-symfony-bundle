import { defineConfig } from "vite";
import symfonyPlugin from "vite-plugin-symfony";
import babel from "vite-plugin-babel";
import eslintPlugin from "vite-plugin-eslint";

export default defineConfig({
    plugins: [
        /* react(), // if you're using React */
        symfonyPlugin(),
        babel(),
        eslintPlugin()
    ],
    root: ".",
    base: "/build/",
    publicDir: false,
    build: {
        manifest: true,
        emptyOutDir: true,
        assetsDir: "",
        outDir: "./public/build",
        rollupOptions: {
            input: {
                app: "./assets/app.js"
            }
        }
    }
});
