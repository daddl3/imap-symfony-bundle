<?php

namespace daddl3\ImapSymfonyBundle\Twig\Runtime;

use daddl3\ImapSymfonyBundle\Service\ImapService;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;
use Twig\Extension\RuntimeExtensionInterface;

readonly class EmailCreationExtensionRuntime implements RuntimeExtensionInterface
{
    public function __construct(
        private ImapService $imapService,
        private Environment $twigEnvironment
    ) {
    }

    /**
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function getEmailInbox(
        ?string $account = null
    ): string {
        $organizedEmails = $this->imapService->getEmailInbox(account: $account);

        return $this->twigEnvironment->render('@Daddl3ImapSymfony/chatbox/chatbox.html.twig', [
            'emailsByAddress' => $organizedEmails,
        ]);
    }
}
